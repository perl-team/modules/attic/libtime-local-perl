Source: libtime-local-perl
Section: perl
Priority: extra
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Jose Luis Rivas <ghostbar@debian.org>,
 Nicholas Bamber <nicholas@periapt.co.uk>
Build-Depends-Indep: perl, libtest-pod-perl, libtest-pod-coverage-perl
Build-Depends: debhelper (>= 7)
Standards-Version: 3.9.1
Homepage: http://search.cpan.org/dist/Time-Local/
Vcs-Git: git://git.debian.org/pkg-perl/packages/libtime-local-perl.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-perl/packages/libtime-local-perl.git

Package: libtime-local-perl
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends}
Description: module to efficiently compute time from local and GMT time
 Time::Local provides functions that are the inverse of built-in perl functions
 localtime() and gmtime(). They accept a date as a six-element array, and return
 the corresponding time(2) value in seconds since the system epoch (Midnight, 
 January 1, 1970 GMT on Unix, for example). This value can be positive or 
 negative, though POSIX only requires support for positive values, so dates 
 before the system's epoch may not work on all operating systems.
